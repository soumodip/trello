// FOR SIGN UP
export const userCreateUrl = "http://138.197.2.164:8080/api/user/signup";

// FOR SIGN IN
export const userValidateUrl = "http://138.197.2.164:8080/api/user/signin";

// CHECK USER AUTH STATUS
export const userSignInStatusUrl = "http://138.197.2.164:8080/api/user/signin/status";

// FOR GETTING & SETTING CONTENTS OF USER ACCOUNT
export const userContentsUrl = "http://138.197.2.164:8080/api/user/contents";

// FOR SIGNING OUT USER
export const userSignOutUrl = "http://138.197.2.164:8080/api/user/signout";


