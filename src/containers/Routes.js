import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import LandingPage from "../components/LandingPage/index";
import SignIn from "../components/UserAuthentication/SignIn";
import SignUp from "../components/UserAuthentication/SignUp";
import Dashboard from "../components/Dashboard/index";
import PageNotFound from "./../components/PageNotFound";
import checkUserSignedIn from "./../components/CheckUserSignedIn";

class Routes extends React.Component {

    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={checkUserSignedIn(LandingPage)} />
                    <Route exact path="/signin" component={checkUserSignedIn(SignIn)} />
                    <Route exact path="/signup" component={checkUserSignedIn(SignUp)} />
                    <Route exact path="/dashboard" component={checkUserSignedIn(Dashboard)} />
                    <Route component={PageNotFound} />
                </Switch>
            </Router>
        )
    }

}

export default Routes;