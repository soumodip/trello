import React, { Component } from "react";
import { userAuthStatus } from "./../actions/UserAuthentication/action"

const checkUserSignedIn = (WrappedComponent) => {
    return class UserSignedIn extends Component {

        componentWillMount() {
            let bearerToken = localStorage.getItem("bearerToken");
            if(bearerToken !== undefined && bearerToken !== null) {
                userAuthStatus(bearerToken).then(
                    (res) => {
                        if(res.data.success) {
                            if((window.location.href).indexOf("/dashboard") === -1) {
                                window.location.href = "/dashboard";
                            }
                        } else {
                            if((window.location.href).indexOf("/dashboard") !== -1) {
                                window.location.href = "/";
                            }
                        }
                    }
                )
            } else {
                if((window.location.href).indexOf("/dashboard") !== -1) {
                    window.location.href = "/";
                }
            }
        }
  
        render() {
            return (
            <div>
                <WrappedComponent {...this.props} />
            </div>
            );
        }

    }
};

export default checkUserSignedIn;