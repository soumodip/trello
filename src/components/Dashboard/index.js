import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import NavBar from "./../Utils/NavBar";
import Screen from "./SubComponents/Screen";
import { getUserContents, setUserContents, saveUserContents } from "./../../actions/Dashboard/action";

class Dashboard extends Component {

    componentWillMount() {
        let bearerToken = localStorage.getItem("bearerToken");
        if(bearerToken !== null) {
            getUserContents(bearerToken).then(
                (res) => {
                    let contents = res.data.data.contents !== null ? res.data.data.contents : "";
                    this.props.saveUserContents(contents);
                }
            )
        }
    }

    onSetUserComponents() {
        let bearerToken = localStorage.getItem("bearerToken");
        if(bearerToken !== null) {
            let contents = this.props.dashboard.categories;
            setUserContents({
                bearerToken: bearerToken,
                contents: contents
            });
        }
    }

    render() {
        return (
            <div>
                <NavBar />
                <Screen />
                <div className="background-light-blue"></div>
                <div className="save-contents" onClick={this.onSetUserComponents.bind(this)}>
                    <i className="fa fa-save" />
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        dashboard: state.dashboard
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        saveUserContents: saveUserContents
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);    