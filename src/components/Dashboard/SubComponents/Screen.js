import React, { Component } from "react";
import Category from "./Category";
import { connect } from "react-redux";

class Screen extends Component {

    render() {
        return (
            <div>
                {
                    (this.props.dashboard.categories).map((category, index) => (
                        (   
                            !category.isDeleted &&
                            <Category key={"category-" + index}
                                      category={category}/>
                        )
                    ))
                }
            </div>
        )
    }

}


function mapStateToProps(state) {
    return {
        dashboard: state.dashboard
    }
}

export default connect(mapStateToProps)(Screen);