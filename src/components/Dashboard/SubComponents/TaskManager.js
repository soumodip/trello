import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import AddTask from "./AddTask";
import Task from "./Task";

class TaskManager extends Component {

    componentWillMount() {
        this.setState({
            category: this.props.category
        });
    }

    componentWillReceiveProps(props) {
        this.setState({
            category: props.category
        });
    }

    render() {
        return (
            <div className="task-manager">
                <Task category={this.state.category} />
                <AddTask category={this.state.category} />
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        dashboard: state.dashboard
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(TaskManager);    