import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { onAddEditTask, onSelectCategoryTask } 
         from "./../../../actions/Dashboard/action";

class AddTask extends Component {

    componentWillMount() {
        this.setState({
            isAddTask: false,
            name: "",
            description: "",
            category: this.props.category
        });
    }

    componentWillReceiveProps(props) {
        this.setState({
            category: props.category
        });
    }

    onToggleAddTask() {
        this.props.onSelectCategoryTask({
            categoryIndex: -1,
            taskIndex: -1
        });
        this.setState({
            isAddTask: !this.state.isAddTask,
            name: "",
            description: ""
        });
    }

    onChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    onAddTask() {
        this.props.onAddEditTask({
            name: this.state.name,
            description: this.state.description,
            categoryIndex: this.state.category.index,
            taskIndex: this.state.category.tasks.length
        });
        this.onToggleAddTask();
    }

    render() {
        return (
            <div className="margin-top-10">
                {(this.state.isAddTask) ? 
                    <div className="add-task align-left">
                        <input className="form-control margin-top-5"
                               name="name"
                               onChange={this.onChange.bind(this)}
                               placeholder="Task Name ..." />
                        <textarea className="form-control margin-top-5"
                                  name="description"
                                  onChange={this.onChange.bind(this)}
                                  placeholder="Task Description ..." ></textarea>
                        <div className="vertical-center">
                            <button className="btn btn-default margin-top-5"
                                    onClick={this.onAddTask.bind(this)}>
                                Add Task
                            </button>
                            <i className="fa fa-times margin-left-10"
                               onClick={this.onToggleAddTask.bind(this)} />
                        </div>
                    </div>
                :
                    <a onClick={this.onToggleAddTask.bind(this)}>+ Add Task</a>
                }
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        dashboard: state.dashboard
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        onAddEditTask: onAddEditTask,
        onSelectCategoryTask: onSelectCategoryTask
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTask);    