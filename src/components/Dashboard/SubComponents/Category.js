import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { onSelectCategoryTask, addEditCategoryName, onDeleteCategory,
         onDragAndDropCategoryTask } 
       from "./../../../actions/Dashboard/action";
import TaskManager from "./TaskManager";

class Category extends Component {

    componentWillMount() {
        this.setState({
            category: this.props.category,
            categoryName: (this.props.category.isEmpty ? "" 
                                                         : 
                                                         this.props.category.name)
        });
    }

    componentWillReceiveProps(props) {
        this.setState({
            category: props.category,
            categoryName: (props.category.isEmpty ? "" 
                                                    : 
                                                    props.category.name)
        });
    }

    onSelectCategoryTask(categoryIndex, taskIndex) {
        this.props.onSelectCategoryTask({
            categoryIndex: categoryIndex, 
            taskIndex: taskIndex
        });
    }

    onChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    onAddEditCategoryName() {
        this.props.addEditCategoryName({
            index: this.state.category.index, 
            name: this.state.categoryName, 
        });
    }

    onKeyPress(event) {
        if(event.key === "Enter") {
            this.onAddEditCategoryName();
        }
    }

    onDeleteCategory() {
        this.props.onDeleteCategory({
            index: this.state.category.index
        });
    }

    // DRAG AND DROP FUNCTIONS

    onDragOver(event) {
        event.preventDefault();
    }

    onDragStart(event) {
        event.dataTransfer.setData("categoryIndex", this.state.category.index);
        event.dataTransfer.setData("taskIndex", -1);
    }

    onDrop(event) {
        event.preventDefault();
        let start = {
            categoryIndex: parseInt(event.dataTransfer.getData("categoryIndex")),
            taskIndex: parseInt(event.dataTransfer.getData("taskIndex"))
        };
        let end = {
            categoryIndex: this.state.category.index,
            taskIndex: -1
        };
        this.props.onDragAndDropCategoryTask({
            start: start,
            end: end
        });
    }

    // END OF DRAG AND DROP FUNCTIONS

    render() {
        let isCategorySelected = (this.props.dashboard.selectedCategoryTask.categoryIndex
                                 === this.state.category.index && 
                                 this.props.dashboard.selectedCategoryTask.taskIndex
                                 === -1)
        return (
            <div className="inline-block">
                {
                    (this.state.category.isEmpty) ?
                        <div className={(isCategorySelected) ?  "category selected align-left"
                                                                : 
                                                                "category empty unselected" 
                                       }>
                            <input name="categoryName" type="text" 
                                   placeholder="Add a Category ..." 
                                   className="form-control"
                                   value={this.state.categoryName}
                                   onChange={this.onChange.bind(this)}
                                   onClick={this.onSelectCategoryTask.bind(this, 
                                                                           this.state.category.index, 
                                                                           -1)} />
                            { isCategorySelected &&
                                <div className="vertical-center margin-top-5">
                                    <button className="btn btn-default inline-block"
                                            onClick={this.onAddEditCategoryName.bind(this)}>
                                            Save
                                    </button>
                                    <i className="fa fa-times inline-block margin-left-10"
                                       onClick={this.onSelectCategoryTask.bind(this, -1, -1)} />
                                </div>
                            }
                        </div>
                    :
                        <div className={"category " + (isCategorySelected ? "selected" : "unselected")}>
                            <div className="row category-details"
                                 draggable={true}
                                 onDragStart={(event) => this.onDragStart(event)}
                                 onDrop={(event) => this.onDrop(event)}
                                 onDragOver={(event) => this.onDragOver(event)}>
                                <div className="col-xs-9 align-left category-name">
                                    <input  name="categoryName" type="text"
                                            className="form-control"
                                            value={this.state.categoryName}
                                            onChange={this.onChange.bind(this)}
                                            onKeyPress={this.onKeyPress.bind(this)}
                                            onClick={this.onSelectCategoryTask.bind(this, 
                                                                                    this.state.category.index, 
                                                                                    -1)} />
                                </div>
                                <div className="col-xs-3 align-right">
                                    <i className="fa fa-trash-o margin-top-5" 
                                       onClick={this.onDeleteCategory.bind(this)}/>
                                </div>
                            </div>
                            <TaskManager category={this.state.category}/>
                        </div>
                }
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        dashboard: state.dashboard
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        onSelectCategoryTask: onSelectCategoryTask,
        addEditCategoryName: addEditCategoryName,
        onDeleteCategory: onDeleteCategory,
        onDragAndDropCategoryTask: onDragAndDropCategoryTask
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Category);