import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { onSelectTask, onAddEditTask, onDeleteTask,
         onDragAndDropCategoryTask } 
         from "./../../../actions/Dashboard/action";

class Task extends Component {

    componentWillMount() {
        this.setState({
            category: this.props.category,
            taskName: "",
            taskDescripton: ""
        });
    }

    componentWillReceiveProps(props) {
        this.setState({
            category: props.category,
            taskName: "",
            taskDescripton: ""
        });
    }

    onChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    onSelectTask(index) {
        this.props.onSelectTask({
            categoryIndex: this.state.category.index,
            taskIndex: index
        });
        this.setState({
            taskName: this.state.category.tasks[index].name,
            taskDescripton: this.state.category.tasks[index].description
        });
    }

    onKeyPress(event, index) {
        if(event.key === "Enter") {
            this.props.onAddEditTask({
                name: this.state.taskName,
                description: this.state.taskDescripton,
                categoryIndex: this.state.category.index,
                taskIndex: index
            });
        }
    }

    onDeleteTask(index) {
        this.props.onDeleteTask({
            categoryIndex: this.state.category.index,
            taskIndex: index
        });
    }

    // DRAG AND DROP FUNCTIONS

    onDragOver(event) {
        event.preventDefault();
    }

    onDragStart(event, index) {
        event.dataTransfer.setData("categoryIndex", this.state.category.index);
        event.dataTransfer.setData("taskIndex", index);
    }

    onDrop(event, index) {
        event.preventDefault();
        let start = {
            categoryIndex: parseInt(event.dataTransfer.getData("categoryIndex")),
            taskIndex: parseInt(event.dataTransfer.getData("taskIndex"))
        }
        let end = {
            categoryIndex: this.state.category.index,
            taskIndex: index
        }
        this.props.onDragAndDropCategoryTask({
            start: start,
            end: end
        });
    }

    // END OF DRAG AND DROP FUNCTIONS

    render() {
        return (
            <div>
                {this.state.category.tasks.map((task, index) => {
                    let isEdit = (this.state.category.index === 
                                  this.props.dashboard.selectedCategoryTask.categoryIndex &&
                                  index === this.props.dashboard.selectedCategoryTask.taskIndex);
                    return (
                        ( !task.isDeleted && 
                            <div    className={"task " + (isEdit ? "edit" : "readonly")}
                                    key={"category-" + this.state.category.index + "task-" + index}
                                    draggable={true}
                                    onDragStart={(event) => this.onDragStart(event, index)}
                                    onDrop={(event) => this.onDrop(event, index)}
                                    onDragOver={(event) => this.onDragOver(event)}>
                                <div className="row">
                                    <div className="col-xs-9">
                                        <input  name="taskName"
                                                onChange={this.onChange.bind(this)}
                                                onClick={this.onSelectTask.bind(this, index)}
                                                className="form-control"
                                                value={task.name}
                                                onKeyPress={this.onKeyPress.bind(this, index)}
                                                placeholder="Enter Name ..." />
                                    </div>
                                    <div className="col-xs-3 align-right">
                                        <i className="fa fa-trash-o" 
                                        onClick={this.onDeleteTask.bind(this, index)}/>
                                    </div>
                                </div>
                                <textarea   name="taskDescription"
                                            onChange={this.onChange.bind(this)}
                                            onClick={this.onSelectTask.bind(this, index)}
                                            className="form-control margin-top-5"
                                            value={task.description}
                                            onKeyPress={this.onKeyPress.bind(this, index)}
                                            placeholder="Enter Description ...">
                                </textarea>
                            </div>
                        )
                    );
                })}
            </div>
        )
    }

};

function mapStateToProps(state) {
    return {
        dashboard: state.dashboard
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        onSelectTask: onSelectTask,
        onAddEditTask: onAddEditTask,
        onDeleteTask: onDeleteTask,
        onDragAndDropCategoryTask: onDragAndDropCategoryTask
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Task);    