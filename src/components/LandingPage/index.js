import React, { Component } from "react";
import NavBar from "./../Utils/NavBar";
import { Link } from "react-router-dom";

class LandingPage extends Component {

    render() {
        return (
            <div>
                <NavBar />
                <div className="block landing-body align-center padding-top-10-per">
                    <h3 className="margin-top-10">Trello lets you work more <br/>collaboratively and get more done.</h3>
                    <p className="margin-top-10">Trello’s boards, lists, and cards enable you to organize<br/> and prioritize your projects in a fun, flexible and rewarding way.</p>
                    <Link to="/signup"><div className="margin-top-20 btn btn-default btn-lg">Sign Up - It's free</div></Link>
                    <p className="margin-top-20">Already use Trello? 
                        <Link to="/signin"><span className="white margin-left-10">Sign In</span></Link>
                    </p>
                </div>
                <div className="background-light-blue"></div>
            </div>
        )
    }

};

export default LandingPage;