import React, { Component } from "react";
import { userSignOut } from "./../../actions/UserAuthentication/action";
import { Link } from "react-router-dom";

class NavBar extends Component {

    onSignOut() {
        let bearerToken = localStorage.getItem("bearerToken");
        userSignOut(bearerToken).then(
            (res) => this.deleteLocalStorage(),
            (err) => this.deleteLocalStorage()
        );
    }

    deleteLocalStorage() {
        localStorage.removeItem("bearerToken");
        window.location.href = "/";
    }

    render() {
        
        return (
            <div className="row landing-header no-margin">
                <div className="col-xs-6 align-left">
                    <img className="height-50" alt="trello-logo" 
                         src="https://d2k1ftgv7pobq7.cloudfront.net/meta/p/res/images/trello-header-logos/af7af6ed478d3460709d715d9b3f74a4/trello-logo-white.svg"/>
                </div>
                { localStorage.getItem("bearerToken") === null ?
                    <div className="col-xs-6 align-right">
                        <div className="height-50 vertical-center">
                            <Link to="/signin"><div className="btn btn-default">Sign In</div></Link>
                            <Link to="/signup"><div className="btn btn-default margin-left-10">Sign Up</div></Link>
                        </div>
                    </div>
                    :
                    <div className="col-xs-6 align-right">
                        <div className="height-50 vertical-center">
                            <a className="btn btn-default"
                               onClick={this.onSignOut.bind(this)}>Sign Out</a>
                        </div>
                    </div>
                }
            </div>
        )
    }

};

export default NavBar;