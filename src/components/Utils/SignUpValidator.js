import validator from 'validator';

export default function signUpValidator(data){

    let errors = {};
  
    if (validator.isEmpty(data.fullName.trim())) {
      errors.fullName = "** Full Name is Required";
    }

    if (validator.isEmpty(data.email.trim())) {
        errors.email = "** Email is Required";
    } else if(!validator.isEmail(data.email.trim())) {
        errors.email = "** Enter a Correct Email";
    }

    if (validator.isEmpty(data.password.trim())) {
        errors.password = "** Password is Required";
    } else if(data.password.trim().length < 6) {
        errors.password = "** Password should be more than 6 letters";
    }

    if (validator.isEmpty(data.confirmPassword.trim())) {
        errors.confirmPassword = "** Confirm Password is Required";
    } else if(data.password.trim() !== data.confirmPassword.trim()) {
        errors.confirmPassword = "** Passwords and Confirm Passwords do not match";
    }
  
    return {
      errors: errors,
      isValid: (Object.keys(errors).length === 0 ? true : false)
    };

}