import React, { Component } from "react";
import TextField from "./../Utils/TextField";
import { userSignIn } from "./../../actions/UserAuthentication/action";
import signInValidator from "./../Utils/SignInValidator";
import { Link } from "react-router-dom";

class SignIn extends Component {

    componentWillMount() {
        this.setState({
            email: "",
            password: "",
            errors: {},
            isSubmitted: false
        });
    }

    onChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    emptyFields(data) {
        this.setState({
            fullName: "",
            email: "",
            password: "",
            confirmPassword: "",
            ...data
        });
    }

    onSubmit() {
        let data = Object.assign({}, this.state);
        let { isValid, errors } = signInValidator(data);
        if( isValid ) {
            userSignIn(data).then(
                (res) => {
                    localStorage.setItem("bearerToken", res.data.bearerToken);
                    window.location.href = "/dashboard";
                },
                (err) => {
                    this.emptyFields({
                        isSubmitted : true,
                        errors: {
                            submit: err.response.data.error
                        }
                    });
                }
            )
        } else {
            this.setState({ errors: errors });
        }
    }

    render() {
        return (
            <div className="width-100-per vertical-center">
                <div className="user-authentication ">
                    <h3 className="heading">Sign in to Trello</h3>
                    <div className="margin-top-10">
                        <TextField
                                field="email"
                                value={this.state.email}
                                label="Email"
                                placeholder="e.g. casandra@cloud.el"
                                error={this.state.errors.email}
                                onChange={this.onChange.bind(this)}
                                />
                    </div>
                    <div className="margin-top-10 margin-bottom-10">
                        <TextField
                                field="password"
                                value={this.state.password}
                                label="Password"
                                placeholder="e.g. **********"
                                type="password"
                                error={this.state.errors.password}
                                onChange={this.onChange.bind(this)}
                                />
                    </div>
                    <button className="btn btn-default inline-block"
                            onClick={this.onSubmit.bind(this)}>
                            Sign In
                    </button>
                    { this.state.isSubmitted &&
                      (!this.state.errors.submit) &&
                        <p className="red margin-top-10">{this.state.errors.submit}</p>
                    }
                    <p className="margin-top-10">
                        Don't have a Trello account ? 
                        <Link to="/signup"><span className="margin-left-10">Sign Up</span></Link>
                    </p>
                </div>
            </div>
        )
    }

}

export default SignIn;