import React, { Component } from "react";
import TextField from "./../Utils/TextField";
import { userSignUp } from "./../../actions/UserAuthentication/action";
import signUpValidator from "./../Utils/SignUpValidator";
import { Link } from "react-router-dom";

class SignUp extends Component {

    componentWillMount() {
        this.setState({
            fullName: "",
            email: "",
            password: "",
            confirmPassword: "",
            errors: {},
            isSubmitted: false
        });
    }

    onChange(event) {
        let data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    emptyFields(data) {
        this.setState({
            fullName: "",
            email: "",
            password: "",
            confirmPassword: "",
            ...data
        });
    }

    onSubmit() {
        let data = Object.assign({}, this.state);
        let { isValid, errors } = signUpValidator(data);
        if( isValid ) {
            userSignUp(data).then(
                (res) => {
                    this.emptyFields({
                        isSubmitted : true
                    });
                },
                (err) => {
                    this.emptyFields({
                        isSubmitted : true,
                        errors: {
                            submit: err.response.data.error
                        }
                    });
                }
            )
        } else {
            this.setState({ errors: errors });
        }
    }

    render() {
        return (
            <div className="width-100-per vertical-center">
                <div className="user-authentication ">
                    <h3 className="heading">Sign up to Trello</h3>
                    <div className="margin-top-10">
                        <TextField
                                field="fullName"
                                value={this.state.fullName}
                                label="Full Name"
                                placeholder="e.g. Fed Mark"
                                error={this.state.errors.fullName}
                                onChange={this.onChange.bind(this)}
                                />
                    </div>
                    <div className="margin-top-10">
                        <TextField
                                field="email"
                                value={this.state.email}
                                label="Email"
                                placeholder="e.g. casandra@cloud.el"
                                error={this.state.errors.email}
                                onChange={this.onChange.bind(this)}
                                />
                    </div>
                    <div className="margin-top-10">
                        <TextField
                                field="password"
                                value={this.state.password}
                                label="Password"
                                type="password"
                                placeholder="e.g. **********"
                                error={this.state.errors.password}
                                onChange={this.onChange.bind(this)}
                                />
                    </div>
                    <div className="margin-top-10 margin-bottom-10">
                        <TextField
                                field="confirmPassword"
                                value={this.state.confirmPassword}
                                label="Confirm Password"
                                type="password"
                                placeholder="e.g. **********"
                                error={this.state.errors.confirmPassword}
                                onChange={this.onChange.bind(this)}
                                />
                    </div>
                    <button className="btn btn-default inline-block"
                            onClick={this.onSubmit.bind(this)}>
                            Sign Up
                    </button>
                    { this.state.isSubmitted &&
                      (!this.state.errors.submit) ?
                            <p className="green margin-top-10">Sign Up completed successfully.</p>
                        :
                            <p className="red margin-top-10">{this.state.errors.submit}</p>
                    }
                    <p className="margin-top-10">
                        Do you have a Trello account ? 
                        <Link to="/signin"><span className="margin-left-10">Sign In</span></Link>
                    </p>
                </div>
            </div>
        )
    }

}

export default SignUp;    