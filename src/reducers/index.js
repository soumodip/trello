import { combineReducers } from "redux";
import dashboardReducer from  "./Dashboard/Reducer";

const combineAllReducers = combineReducers({
    dashboard: dashboardReducer
});

export default combineAllReducers;