import update from "immutability-helper";

export const selectedCategoryTask = (state, data) => {
    // CHANGE SELECTED DATA
    return Object.assign({}, state, {
        selectedCategoryTask: data
    });
};

export const addEditCategoryName = (state, data) => {
    let category = {
        index: data.index,
        isEmpty: false,
        isDeleted: false,
        name: data.name,
        tasks: []
    };
    let newCategory;
    let updatedState = state;
    if(state.categories[data.index].isEmpty) {
        newCategory = {
            index: data.index + 1,
            isEmpty: true
        }
        updatedState = update(state,
            { 
                categories: { $push: [newCategory] }
            }
        );
    } else {
        category.tasks = state.categories[data.index].tasks;
    }
    updatedState = update(updatedState,
        { 
            categories: { [data.index]: { $set: category } }
        }
    );
    // CHANGE SELECTED DATA
    return Object.assign({}, updatedState, {
        selectedCategoryTask: {
            categoryIndex: -1,
            taskIndex: -1
        }
    });
};

export const onDeleteCategory = (state, data) => {
    let updatedState = update(state, {
        categories: { [data.index]: { isDeleted: { $set: true } } }
    });
    return Object.assign({}, updatedState, {
        selectedCategoryTask: {
            categoryIndex: -1,
            taskIndex: -1
        }
    });
};

export const onAddEditTask = (state, data) => {
    let task = {
        index: data.taskIndex,
        name: data.name,
        description: data.description,
        isDeleted: false
    };
    let updatedState;
    if(state.categories[data.categoryIndex].tasks[data.taskIndex] !== undefined) {
        updatedState = update(state, {
            categories: { 
                [data.categoryIndex]: { tasks: { 
                    [data.taskIndex]: { $set: [task] }
                } } }
        });
    } else {
        updatedState = update(state, {
            categories: { 
                [data.categoryIndex]: { tasks: { $push: [task] } } }
        });
    }
    return Object.assign({}, updatedState, {
        selectedCategoryTask: {
            categoryIndex: -1,
            taskIndex: -1
        }
    });
};

export const onSelectTask = (state, data) => {
    return Object.assign({}, state, {
        selectedCategoryTask: {
            categoryIndex: data.categoryIndex,
            taskIndex: data.taskIndex
        }
    });
};

export const onDeleteTask = (state, data) => {
    let task = Object.assign({}, state.categories[data.categoryIndex].tasks[data.taskIndex]);
    task.isDeleted = true;
    let updatedState = update(state, {
        categories: { 
            [data.categoryIndex]: { tasks: { 
                [data.taskIndex]: { $set: task }
            } } }
    });
    return Object.assign({}, updatedState, {
        selectedCategoryTask: {
            categoryIndex: -1,
            taskIndex: -1
        }
    });
};

export const onDragAndDropCategoryTask = (state, data) => {
    let updatedState;
    if(data.start.taskIndex === -1) {
        // CATEGORY TO CATEGORY CHANGE
        let startCategory = Object.assign({}, state.categories[data.start.categoryIndex]);
        let endCategory = Object.assign({}, state.categories[data.end.categoryIndex]);
        updatedState = update(state, {
            categories: {
                [data.start.categoryIndex]: {$set: {...endCategory, index: data.start.categoryIndex}},
                [data.end.categoryIndex]: {$set: {...startCategory, index: data.end.categoryIndex}}
            }
        });
    } else {
        if(data.end.taskIndex === -1) {
            // TASK TO CATEGORY CHANGE
            let updatedIndex = state.categories[data.end.categoryIndex].tasks.length;
            let startTask = Object.assign({}, state.categories[data.start.categoryIndex].tasks[data.start.taskIndex]);
            updatedState = update(state, {
                categories: {
                    [data.end.categoryIndex]: {
                        tasks: {$push: [{...startTask, index: updatedIndex}]}
                    },
                    [data.start.categoryIndex]: {
                        tasks: {
                            [data.start.taskIndex]: {$set: {...startTask, isDeleted: true}}
                        }
                    }
                }
            });
        } else {
            // TASK TO TASK CHANGE
            let startTask = Object.assign({}, state.categories[data.start.categoryIndex].tasks[data.start.taskIndex]);
            let endTask = Object.assign({}, state.categories[data.end.categoryIndex].tasks[data.end.taskIndex]);
            if(data.start.categoryIndex === data.end.categoryIndex) {
                updatedState = update(state, {
                    categories: {
                        [data.start.categoryIndex]: {
                            tasks: {
                                [data.start.taskIndex]: {$set: {...endTask, index: data.start.taskIndex}},
                                [data.end.taskIndex]: {$set: {...startTask, index: data.end.taskIndex}}
                            }
                        }
                    }
                });
            } else {
                updatedState = update(state, {
                    categories: {
                        [data.start.categoryIndex]: {
                            tasks: {
                                [data.start.taskIndex]: {$set: endTask}
                            }
                        },
                        [data.end.categoryIndex]: {
                            tasks: {
                                [data.end.taskIndex]: {$set: startTask}
                            }
                        }
                    }
                });
            }
        }
    }
    return Object.assign({}, updatedState, {
        selectedCategoryTask: {
            categoryIndex: -1,
            taskIndex: -1
        }
    });
}

export const saveUserContents = (state, data) => {
    let contents;
    if(data === "" || data === undefined) {
        contents = [
            {
                index: 0,
                isEmpty: true,
                isDeleted: false
            }
        ]
    } else {
        contents = JSON.parse(data);
    }
    return Object.assign({}, state, {
        categories: contents
    });
}