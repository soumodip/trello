import  dashboardState from './State';
import { selectedCategoryTask, addEditCategoryName, onDeleteCategory, 
         onSelectTask, onAddEditTask, onDeleteTask,
         onDragAndDropCategoryTask, saveUserContents } 
        from "./Helper";

function dashboardReducer( state = dashboardState, action ) {
    switch(action.type) {
        case "ON_SELECTED_CATEGORY_TASK":
            return selectedCategoryTask(state, action.payload);
        case "ADD_EDIT_CATEGORY_NAME":
            return addEditCategoryName(state, action.payload);
        case "DELETE_CATEGORY":
            return onDeleteCategory(state, action.payload);
        case "ADD_EDIT_TASK":
            return onAddEditTask(state, action.payload);
        case "ON_SELECT_TASK":
            return onSelectTask(state, action.payload);
        case "ON_DELETE_TASK":
            return onDeleteTask(state, action.payload);
        case "ON_DRAG_AND_DROP_CATEGORY_TASK":
            return onDragAndDropCategoryTask(state, action.payload);
        case "SAVE_USER_CONTENTS":
            return saveUserContents(state, action.payload);
        default:
            return state;
    }
}

export default dashboardReducer;