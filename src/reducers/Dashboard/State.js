const dashboardState = {
    selectedCategoryTask: {
        categoryIndex: -1,
        taskIndex: -1
    },
    categories: [
        {
            index: 0,
            isEmpty: true,
            isDeleted: false
        }
    ]
};

export default dashboardState;