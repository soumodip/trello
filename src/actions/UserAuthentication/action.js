import axios from 'axios';
import { userCreateUrl, userValidateUrl, userSignInStatusUrl, 
         userSignOutUrl } from "../../utils/apiConstants";

export const userSignUp = (params = {}) => {
    return axios.put(userCreateUrl,  params);
};

export const userSignIn = (params = {}) => {
    return axios.post(userValidateUrl, params);
};

export const userAuthStatus = (bearerToken) => {
    return axios.get(userSignInStatusUrl + "?bearerToken=" + bearerToken);
};

export const userSignOut = (bearerToken) => {
    return axios.get(userSignOutUrl + "?bearerToken=" + bearerToken);
};