import axios from "axios";
import { userContentsUrl } from "./../../utils/apiConstants";

export const onSelectCategoryTask = (data) => {
    return {
        type: "ON_SELECTED_CATEGORY_TASK",
        payload: data
    };
};

export const addEditCategoryName = (data) => {
    return {
        type: "ADD_EDIT_CATEGORY_NAME",
        payload: data
    };
};

export const onDeleteCategory = (data) => {
    return {
        type: "DELETE_CATEGORY",
        payload: data
    };
};

export const onAddEditTask = (data) => {
    return {
        type: "ADD_EDIT_TASK",
        payload: data
    };
};

export const onSelectTask = (data) => {
    return {
        type: "ON_SELECT_TASK",
        payload: data
    };
};

export const onDeleteTask = (data) => {
    return {
        type: "ON_DELETE_TASK",
        payload: data
    };
};

export const onDragAndDropCategoryTask = (data) => {
    return {
        type: "ON_DRAG_AND_DROP_CATEGORY_TASK",
        payload: data
    };
};

export const getUserContents = (bearerToken) => {
    return axios.get(userContentsUrl + "?bearerToken=" + bearerToken);
};

export const setUserContents = (data) => {
    return axios({
        method: "post",
        url: userContentsUrl,
        data: data
    })
};

export const saveUserContents = (data) => {
    return {
        type: "SAVE_USER_CONTENTS",
        payload: data
    };
};