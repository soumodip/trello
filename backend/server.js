import express from "express";
import bodyParser from "body-parser";
import session from "express-session";

let app = express();
// TO ACCEPT PUT, POST AND DELETE AND OTHER REQUEST
app.use(bodyParser.json());

//SETTING EXPRESS SESSION CREDENTIALS
var sessionCredentials = {
    secret: 'SECRET',
    cookie: {maxAge: 60*60*1000},
    resave: true,
    saveUninitialized: false
};

if(app.get('env') === 'production') {
    app.set('trust proxy', 1);
    sessionCredentials.cookie.secure = true;
}

app.use(session(sessionCredentials));

// ALLOW CORS - ENABLE THIS IF THIS SERVER IS DIFFERENT THAN THE CLIENT SERVER
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    next();
});

app.listen(8080, ()=> {
    console.log("TRELLO CONNECTION STARTED");
});

// FOR MONGO DB CONNECTION
import { connect } from "./services/Mongoose";
// CALL CONNECT TO START MONGODB CONNECTION
connect();

// IMPORT ALL ROUTES
import { createUser, validateUser, getSignInStatus, getContents, setContents, signOutUser } from './controllers/Routes';

// FOLLOWING CRUD PRINCIPLES - GET - FETCH, PUT - ADD, 
// POST - EDIT AND DELETE - REMOVE

// CREATE A USER
app.put("/api/user/signup", createUser);
// VALIDATE A USER
app.post("/api/user/signin", validateUser);
// GET SIGN IN STATUS OF THE USER
app.get("/api/user/signin/status", getSignInStatus);

// GET ALL CONTENT FROM USER ACCOUNT
app.get("/api/user/contents", getContents);
// SETS CONTENTS TO USER ACCOUNT
app.post("/api/user/contents", setContents);
// SIGNS OUT THE USER'S SESSION
app.get("/api/user/signout", signOutUser);