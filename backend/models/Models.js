import mongoose from "mongoose";

const userSchema = mongoose.Schema({
    fullName: String,
    email: String,
    password: String,
    bearerToken: String,
    contents: String,
    createdAt: Date
});

const userCollection = mongoose.model("user", userSchema);

export default userCollection;