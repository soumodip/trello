import mongoose from "mongoose";

exports.connect = () => {

    // CONNECT TO MONGODB DATABASE
    // mongoose.connect('mongodb://localhost:27017/trello'); // FOR LOCAL  
    mongoose.connect('mongodb://mongodbuser:mongodbpassword@ds133776.mlab.com:33776/trello'); // FOR PRODUCTION
    let db = mongoose.connection;
    db.on("error", console.error.bind(console, "MONGOOSE CONNECT ERROR : "));
    db.once("openUri", function() {
      console.log("MONGO DB CONNECTED");
    });

};