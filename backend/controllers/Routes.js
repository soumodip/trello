import userCollection from "./../models/Models";
import { userCreateValidate, userAuthValidate } from "./Helper";
import uuid from "uuid/v4";

// CREATE A USER
exports.createUser = (req, res) => {
    let options = { upsert: true };
    let { user, isValid, error } = userCreateValidate(req);
    if(isValid) {
        user.bearerToken = uuid();
        user.contents = "";
        userCollection.update(
            {email: req.body.email}, 
            {$setOnInsert: user}, 
            options, 
            function(err, numUpdated) {
                if(err) {
                    res.status(500).json({
                        error: err
                    });
                } else {
                    if(numUpdated.upserted === undefined) {
                        res.status(400).json({
                            error: "User already exists in the database."
                        });
                    } else {
                        res.status(200).json({
                            success: true
                        });
                    }
                }
            }
        );
    } else {
        res.status(500).json({
            error: error
        });
    }
};

// VALIDATE A USER
exports.validateUser = (req, res) => {
    let { user, isValid, error } = userAuthValidate(req);
    if(isValid) {
        let bearerToken = uuid();
        userCollection.findOneAndUpdate({ email: user.email, password: user.password }, 
                                        { bearerToken: bearerToken },
                                        function(err, body){
            if(err) {
                res.status(500).json({
                    error: err
                });
            } else {
                if(body === null) {
                    res.status(400).json({
                        error: "User does not exist in the database."
                    });
                } else {
                    // SET CREDENTIALS TO PROOF USER LOGGED IN
                    res.status(200).json({
                        success: true,
                        bearerToken: bearerToken
                    });
                }
            }
        });
    } else {
        res.status(500).json({
            error: error
        });
    }
    
};

// GET CURRENT SIGN IN STATUS
exports.getSignInStatus = (req, res) => {
    let bearerToken = req.query.bearerToken;
    userCollection.findOne({ bearerToken: bearerToken }, function(err, body){
        if(err) {
            res.status(400).json({
                success: false
            });
        } else {
            if(body === null) {
                res.status(200).json({
                    success: false
                });
            } else {
                res.status(200).json({
                    success: true
                });
            }
        }
    });
}

// GET ALL CONTENT FROM USER ACCOUNT
exports.getContents = (req, res) => {
    let bearerToken = req.query.bearerToken;
    userCollection.findOne({ bearerToken: bearerToken }, function(err, body){
        if(err) {
            res.status(500).json({
                error: err
            });
        } else {
            res.status(200).json({
                success: true,
                data: body
            });
        }
    });
};

// SETS CONTENTS TO USER ACCOUNT
exports.setContents = (req, res) => {
    let bearerToken = req.body.bearerToken;
    userCollection.update({ bearerToken: bearerToken }, { contents: JSON.stringify(req.body.contents) },
                            function(err, body){
        if(err) {
            res.status(500).json({
                error: err
            });
        } else {
            res.status(200).json({
                success: true,
                data: body
            });
        }
    });
};

// SIGNS OUT THE USER'S SESSION
exports.signOutUser = (req, res) => {
    let updatedBearerToken = uuid();
    let bearerToken = req.query.bearerToken;
    userCollection.update({ bearerToken: bearerToken }, { bearerToken: updatedBearerToken },
                            function(err, body){
        if(err) {
            res.status(500).json({
                error: err
            });
        } else {
            res.status(200).json({
                success: true
            });
        }
    });
};