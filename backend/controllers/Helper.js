import validator from "validator";

exports.userCreateValidate = (req) => {

    let isValid = true;
    let error = {};

    if(validator.isEmpty((req.body.email).trim())) {
        error.email = "Email is empty";
        isValid = false;
    } else if(!validator.isEmail((req.body.email).trim())) {
        error.email = "Email is invalid";
        isValid = false;
    }

    if(validator.isEmpty((req.body.password).trim())) {
        error.email = "Password is empty";
        isValid = false;
    } else if((req.body.password).trim().length < 6) {
        error.email = "Password is less than 6 charecters";
        isValid = false;
    }

    if(validator.isEmpty((req.body.fullName).trim())) {
        error.email = "Full Name is empty";
        isValid = false;
    }

    return {
        isValid: isValid,
        error: error,
        user: {
            email: (req.body.email).trim(),
            password: (req.body.password).trim(),
            fullName: (req.body.fullName).trim()
        }
    }

};

exports.userAuthValidate = (req) => {

    let isValid = true;
    let error = {};

    if(validator.isEmpty((req.body.email).trim())) {
        error.email = "Email is empty";
        isValid = false;
    } else if(!validator.isEmail((req.body.email).trim())) {
        error.email = "Email is invalid";
        isValid = false;
    }

    if(validator.isEmpty((req.body.password).trim())) {
        error.email = "Password is empty";
        isValid = false;
    } else if((req.body.password).trim().length < 6) {
        error.email = "Password is less than 6 charecters";
        isValid = false;
    }

    return {
        isValid: isValid,
        error: error,
        user: {
            email: (req.body.email).trim(),
            password: (req.body.password).trim()
        }
    }

}